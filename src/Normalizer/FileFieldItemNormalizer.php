<?php

namespace Drupal\value\Normalizer;

use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldType\FileItem;

class FileFieldItemNormalizer extends FieldItemNormalizer {

  /**
   * {@inheritdoc}
   */
  protected $supportedInterfaceOrClass = [FileItem::class];

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []): array|string|int|float|bool|\ArrayObject|NULL {
    $attributes = parent::normalize($object, $format, $context);

    /** @var FileInterface $file */
    if ($file = $object->get('entity')->getValue()) {
      $attributes['uri'] = $file->getFileUri();
      $attributes['url'] = \Drupal::service('file_url_generator')->generateAbsoluteString($attributes['uri']);
    }

    return $attributes;
  }
}
