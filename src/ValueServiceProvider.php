<?php

namespace Drupal\value;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

class ValueServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $app_root = '%app.root%';
    if (version_compare(\Drupal::VERSION, '9.0.0', '<')) {
      $app_root = new Reference('app.root');
    }

    $container->getDefinition('theme.manager')
      ->setClass(ThemeManager::class)
    ->setArguments([$app_root, new Reference('theme.negotiator'), new Reference('theme.initialization'), new Reference('module_handler'), new Reference('language_manager')]);
  }

}
